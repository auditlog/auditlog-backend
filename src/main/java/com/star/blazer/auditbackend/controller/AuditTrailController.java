package com.star.blazer.auditbackend.controller;

import com.star.blazer.auditbackend.datamodel.AuditInput;
import com.star.blazer.auditbackend.datamodel.AuditObject;
import com.star.blazer.auditbackend.repository.AuditRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.ConnectException;
import java.util.Date;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/audit")
@Slf4j
public class AuditTrailController {

    @Autowired
    private AuditRepo repo;

    @PostMapping("/add")
    public ResponseEntity addAuditLog(@RequestBody AuditInput input){
        log.info("Adding into Redis!");
        try {
            String uuid = UUID.randomUUID().toString();
            repo.save(new AuditObject(uuid, new Date(), input.getAction(), input.getUsername()));
            return ok().body(repo.findOne(uuid));
        }catch (Exception e){
            log.info("Uh oh, facing errors now.");
            return status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("/inquiry")
    public ResponseEntity findAuditLog(@RequestParam(defaultValue = "none") String filter){
        return switchFilter(filter, "");
    }

    @GetMapping("/{username}/inquiry")
    public ResponseEntity findAuditLogWithUsername(@PathVariable String username, @RequestParam(defaultValue = "none") String filter){
        log.info("Inquiry with username detected: {}", username);
        return switchFilter(filter, username);
    }

    @PostMapping(value = "/placeholder", consumes = "application/json", produces = "application/json")
    public ResponseEntity placeholder(@RequestBody AuditInput body){
        log.info(body.toString());
        return ok().body(body);
    }

    public ResponseEntity switchFilter(String filter, String username){
        try {
            switch(filter){
                case "none":
                    log.info("No filter set. Processing.");
                    return username.isEmpty() ? ok().body(repo.findAll()) : ok().body(repo.getAllByUsernameEquals(username));
                case "LOGIN":
                    return  username.isEmpty() ? ok().body(repo.getAllByAction_Login()) : ok().body(repo.getAllByUsernameAndActionEquals(username, filter));
                case "CLICK":
                    return username.isEmpty() ? ok().body(repo.getAllByAction_Click()) : ok().body(repo.getAllByUsernameAndActionEquals(username,filter));
                default:
                    return status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve data from Redis :(");
            }
        }catch (Exception e){
            return status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to retrieve data from Redis.");
        }
    }


}
