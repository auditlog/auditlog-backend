package com.star.blazer.auditbackend.datamodel;

import com.google.gson.Gson;
import com.star.blazer.auditbackend.config.EnumList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuditInput {

    private EnumList.ACTIONLIST action;
    private String username;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
